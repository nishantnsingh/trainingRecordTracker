
<%@page language="java"%>
<%@page import="java.sql.*"%>

<jsp:include page="mainPage.html" />

<%!
  Connection con;
  PreparedStatement ps;
  PreparedStatement ps1;
  ResultSet rs;
%>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>



      <link rel="stylesheet" href="style/style.css">
</head>

<body>

  <div class="group_grid">
    <form method="post" class="form login">
      <header class="group__header">
        <h3 class="group__title">Training Details</h3>
      </header>
      <div class="group__body">
        <%
        String employeeId = request.getParameter("employeeId");
        try {
          Class.forName("com.mysql.jdbc.Driver");
          con=DriverManager.getConnection("jdbc:mysql:///db_safetyTraining","root","root1234");
          int i=1;
          ps1=con.prepareStatement("select COUNT(trainingDetails) from trainingDetails where employeeId = ?");
          ps1.setString(1, employeeId);
          rs=ps1.executeQuery();
          while(rs.next()){
            i = i+1;
            
          } %>
          <table style="width:100%">
              <tr style="
              text-align:  left; font-size: 20px;" >
                <th>Total Number of Trainings</th>
                <td style="
                text-align:  left;">
          <%=i%></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr>
          <th style="
          text-align:  left;">Training Name</th>
          <th style="
          text-align:  left;">Training Date</th>
          <th style="
          text-align:  left;">Training Duration</th>
        </tr>
          <%
          ps=con.prepareStatement("select * from trainingDetails where employeeId = ? ORDER BY id DESC");
          ps.setString(1, employeeId);
          rs=ps.executeQuery();
          while(rs.next()){
            %>
            
            <tr>
                <td><%=rs.getString("trainingDetails")%> </td>
            <td><%=rs.getString("trainingDate")%></td><td><%=rs.getString("trainingDuration")%>
            </td>
            <%
          }
        }catch(Exception e) {
          out.println(e);
        }
        %>
      </div>
    </form>
  </div>
 </body>
</html>