
<jsp:include page="mainPage.html" />


<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>



      <link rel="stylesheet" href="style/style.css">


</head>

<body>
  <div class="group_grid">

    <form  method="post" action="editByEmpValidation.jsp" class="form group">

      <header class="group__header">
        <h3 class="group__title">Edit Training Details</h3>
      </header>

      <div class="group__body">
        <div class="form__field">
          <input type="text" placeholder="Enter Employee Id" required name="employeeId">
          <input type="text" placeholder="Enter Training Date" required name="trainingDate">
        </div></div>
        <footer class="group__footer">
        <input type="submit" value="Click to edit Training Details">
        </footer>

    </form>
  </div>
 </body>
</html>