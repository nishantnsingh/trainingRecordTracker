<%@ page import="java.sql.*" %>


<%!
	Connection con;
	PreparedStatement ps;
	ResultSet rs;
%>

<%
		String employeeId = request.getParameter("employeeId");
		String employeeName=request.getParameter("employeeName");
		String employeeDepartment=request.getParameter("employeeDepartment");
		String trainingDate=request.getParameter("trainingDate");
		String trainingDetails=request.getParameter("trainingDetails");
		String trainingDuration=request.getParameter("trainingDuration");

		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql:///db_safetyTraining","root","root1234");
				ps=con.prepareStatement("insert into trainingDetails(employeeId, employeeName, trainingDate, trainingDetails, employeeDepartment, trainingDuration) values(?, ?, ?, ?, ?, ?)");
				ps.setString(1,employeeId);
				ps.setString(2,employeeName);
                ps.setString(3,trainingDate);
				ps.setString(4,trainingDetails);
				ps.setString(5, employeeDepartment);
				ps.setString(6, trainingDuration);
				ps.executeUpdate();
				con.close();
				
				response.sendRedirect("addTrainingData.jsp");

		}
		catch(Exception e)
		{
			out.print(e);
		}
%>
