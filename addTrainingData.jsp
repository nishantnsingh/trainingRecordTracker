<jsp:include page="mainPage.html" />

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>



      <link rel="stylesheet" href="css/style.css">


</head>

<body>
  <div class="group_grid">

    <form  method="post" action = "addValidation.jsp" class="form group">

      <header class="group__header">
        <h3 class="group__title">Add Training Details</h3>
      </header>

      <div class="group__body">
         <input type="text" placeholder="Enter Employee Id" required name="employeeId"><br>
         <input type="text" placeholder="Enter Employee Name" required name="employeeName"><br>
         <input type="text" placeholder="Enter Department" required name="employeeDepartment"><br>
         <input type="text" placeholder="Enter Training Date (dd/mm/yyyy)" required name="trainingDate"><br>
         <input type="text" placeholder="Enter Training Details" required name="trainingDetails"><br>
         <input type="text" placeholder="Enter Training Duration" required name="trainingDuration"><br>
<br>
        </div>
        <footer class="group__footer">
        <input type="submit" value="Click to submit">
        </footer>
    </form>
  </div>
 </body>
</html>
