
<%@page language="java"%>
<%@page import="java.sql.*"%>

<jsp:include page="mainPage.html" />

<%!
  Connection con;
  PreparedStatement ps;
  PreparedStatement ps1;

  ResultSet rs;
%>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>



      <link rel="stylesheet" href="style/style.css">
</head>

<body>

  <div class="group_grid">
    <form method="post" class="form login">
      <header class="group__header">
        <h3 class="group__title">Training Details</h3>
      </header>
      <div class="group__body">
        <%
        String trainingDetails = request.getParameter("trainingDetails");
        try {
          Class.forName("com.mysql.jdbc.Driver");
          con=DriverManager.getConnection("jdbc:mysql:///db_safetyTraining","root","root1234");
          int i=0;
          ps1=con.prepareStatement("select COUNT(employeeName) from trainingDetails where trainingDetails = ?");
          ps1.setString(1, trainingDetails);
          rs=ps1.executeQuery();
          while(rs.next()){
            i = i+1;
            
          }
          %>
          <table style="width:100%">
            <tr style="
            text-align:  left; font-size: 20px;">
              <th>Total Number of Employees</th>
              <td>
                  <%=i%></td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr> 
            <tr>
              <th style="
              text-align:  left;">Employee Name</th>
              <th style="
              text-align:  left;">Training Date</th>
              <th style="
              text-align:  left;">Training Duration</th>

            </tr>
          <%
          ps=con.prepareStatement("select * from trainingDetails where trainingDetails = ?");
          ps.setString(1, trainingDetails);
          rs=ps.executeQuery();
          while(rs.next()){
            %>
            <tr>
            <td><%=rs.getString("employeeName")%></td>
            <td><%=rs.getString("trainingDate")%></td>
            <td><%=rs.getString("trainingDuration")%>
          </tr>
            <%
          }%>
        </table>
          <%
        }catch(Exception e) {
          out.println(e);
        }
        %>
      </div>
    </form>
  </div>
 </body>
</html>