
<%@page language="java"%>
<%@page import="java.sql.*"%>

<jsp:include page="mainPage.html" />

<%!
  Connection con;
  PreparedStatement ps;
  PreparedStatement ps1;

  ResultSet rs;
%>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Teacher group</title>



      <link rel="stylesheet" href="style/style.css">
</head>

<body>

  <div class="group_grid">
    <form method="post" class="form login" action="update.jsp" >
      <header class="group__header">
        <h3 class="group__title">Training Details</h3>
      </header>
      <div class="group__body">
        <%
        String employeeId = request.getParameter("employeeId");
        String trainingDate = request.getParameter("trainingDate");
        try {
          Class.forName("com.mysql.jdbc.Driver");
          con=DriverManager.getConnection("jdbc:mysql:///db_safetyTraining","root","root1234");
          ps=con.prepareStatement("select * from trainingDetails where employeeId = ? and trainingDate = ?");
          ps.setString(1, employeeId);
          ps.setString(2, trainingDate);
          rs=ps.executeQuery();
          while(rs.next()){
            %>
            <label style="font-weight: bolder; font-size: 14px;">Employee Name</label>
            <input type="text" readonly required name="employeeName" value="<%=rs.getString("employeeName")%>"><br><br>
            <label style="font-weight: bolder; font-size: 14px;">Employee Department</label>
            <input type="text"  required name="employeeDepartment" value="<%=rs.getString("employeeDepartment")%>"><br><br>

            <label style="font-weight: bolder; font-size: 14px;">Training Date</label>
            <input type="text" readonly required name="trainingDate" value="<%=rs.getString("trainingDate")%>"><br><br>
            <label style="font-weight: bolder; font-size: 14px;">Training Duration</label>
            <input type="text"  required name="trainingDuration" value="<%=rs.getString("trainingDuration")%>"><br><br>
            <label style="font-weight: bolder; font-size: 14px;">Training Details</label>
            <input type="text"  required name="trainingDetails" value="<%=rs.getString("trainingDetails")%>"><br><br>

            
                    <input type="submit" value="Click to edit Details">        
                
            <%
          }
        }catch(Exception e) {
          out.println(e);
        }
        %>
    </div>
    </form>
  </div>
 </body>
</html>